﻿namespace RacoonWork.Application.Candidates.Models
{
    public class ApplicationStatusModel
    {
        public int Id { get; set; }
        public int VacancyId { get; set; }

        public int CandidateId { get; set; }

        public int ApplicationStatusId { get; set; }
    }
}
