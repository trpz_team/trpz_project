﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Candidates.Commands
{
    public class ApplyForVacancy:IRequest<ApplicationStatusModel>
    {
        public int CandidateId { get; set; }

        public int VacancyId { get; set; }

        class Validator : AbstractValidator<ApplyForVacancy>
        {
            public Validator()
            {
                RuleFor(x => x.CandidateId).NotEmpty();
                RuleFor(x => x.VacancyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<ApplyForVacancy, ApplicationStatusModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ApplicationStatusModel> Handle(ApplyForVacancy request, CancellationToken cancellationToken)
            {
                var applicationStatus = mapper.Map<CandidateApplicationStatus>(request);
                applicationStatus.ApplicationStatusId = 1;// submitted
                dbContext.CandidateApplicationStatuses.Add(applicationStatus);
                await dbContext.SaveChangesAsync();
                return mapper.Map<ApplicationStatusModel>(applicationStatus);
            }
        }
    }
}
