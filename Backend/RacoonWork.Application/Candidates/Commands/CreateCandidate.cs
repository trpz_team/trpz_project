﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Candidates.Commands
{
    public class CreateCandidate : IRequest<CandidateModel>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }

        public class Validator : AbstractValidator<CreateCandidate>
        {
            public Validator()
            {
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.Email).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateCandidate, CandidateModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CandidateModel> Handle(CreateCandidate request, CancellationToken cancellationToken)
            {
                var candidate = mapper.Map<Candidate>(request);
                dbContext.Candidates.Add(candidate);
                await dbContext.SaveChangesAsync(cancellationToken);
                return mapper.Map<CandidateModel>(candidate);
            }
        }

    }
}
