﻿using MediatR;
using RacoonWork.Application.Candidates.Models;

namespace RacoonWork.Application.Candidates.Commands
{
    public class UploadCandidateCv :IRequest<CandidateModel>
    {
    }
}
