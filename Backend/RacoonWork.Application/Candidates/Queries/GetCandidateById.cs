﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Candidates.Queries
{
    public class GetCandidateById : IRequest<CandidateModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetCandidateById>
        {
            public Validator()
            {
                RuleFor(x => x.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetCandidateById, CandidateModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CandidateModel> Handle(GetCandidateById request, CancellationToken cancellationToken)
            {
                var candidate = await dbContext.Candidates.FindAsync(new object[] { request.Id }, cancellationToken);
                return mapper.Map<CandidateModel>(candidate);
            }
        }
    }
}
