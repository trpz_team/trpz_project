﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace RacoonWork.Application.Candidates.Queries
{
    public class GetAllApplications : IRequest<ApplicationsListModel>
    {
        public int CandidateId { get; set; }

        public class Validator : AbstractValidator<GetAllApplications>
        {
            public Validator()
            {
                RuleFor(x => x.CandidateId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetAllApplications, ApplicationsListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ApplicationsListModel> Handle(GetAllApplications request, CancellationToken cancellationToken)
            {
                var allApplications =await dbContext.CandidateApplicationStatuses
                    .Where(x => x.CandidateId == request.CandidateId)
                    .ToListAsync(cancellationToken);

                var mappedApplications = allApplications
                    .Select(x => mapper.Map<ApplicationStatusModel>(x));

                return new ApplicationsListModel { Items = mappedApplications };
            }
        }
    }
}
