﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Candidates.Queries
{
    public class GetApplicationStatus : IRequest<ApplicationStatusModel>
    {
        public int CandidateId { get; set; }
        public int VacancyId { get; set; }

        public class Validator : AbstractValidator<GetApplicationStatus>
        {
            public Validator()
            {
                RuleFor(x => x.CandidateId).NotEmpty();
                RuleFor(x => x.VacancyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetApplicationStatus, ApplicationStatusModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<ApplicationStatusModel> Handle(GetApplicationStatus request, CancellationToken cancellationToken)
            {
                var appStatus = await this.dbContext
                    .CandidateApplicationStatuses
                    .FirstOrDefaultAsync(x => x.CandidateId == request.CandidateId &&
                        x.VacancyId == request.VacancyId);

                return mapper.Map<ApplicationStatusModel>(appStatus);
            }
        }
    }
}
