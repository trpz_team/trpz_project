﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Vacancies.Models;
using RacoonWork.Persistence;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Candidates.Queries
{
    public class GetAdvisedVacancies : IRequest<VacanciesListModel>
    {
        public int CandidateId { get; set; }

        public class Validator : AbstractValidator<GetAdvisedVacancies>
        {
            public Validator()
            {
                RuleFor(x => x.CandidateId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetAdvisedVacancies, VacanciesListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }
            public async Task<VacanciesListModel> Handle(GetAdvisedVacancies request, CancellationToken cancellationToken)
            {
                var candidate = await dbContext.Candidates
                    .FindAsync(new object[] { request.CandidateId }, cancellationToken);

                var vacancies = dbContext.Vacancies
                    .Where(x => x.City == candidate.City)
                    .Select(vacancy => mapper.Map<VacancyModel>(vacancy));

                return new VacanciesListModel { Items = vacancies };
            }
        }
    }
}
