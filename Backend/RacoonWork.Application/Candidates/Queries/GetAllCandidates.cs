﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace RacoonWork.Application.Candidates.Queries
{
    public class GetAllCandidates : IRequest<CandidateListModel>
    {
        public class Handler : IRequestHandler<GetAllCandidates, CandidateListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CandidateListModel> Handle(GetAllCandidates request, CancellationToken cancellationToken)
            {
                var candidates = await dbContext.Candidates.ToListAsync(cancellationToken);
                var candidateModels = candidates.Select(x => mapper.Map<CandidateModel>(x));
                return new CandidateListModel { Items = candidateModels };
            }
        }
    }
}
