﻿using AutoMapper;
using RacoonWork.Application.Candidates.Commands;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Domain.Entities;

namespace RacoonWork.Application.Infrastructure.AutoMapper.MappingProfiles
{
    class CandidateMappingProfile : Profile
    {
        public CandidateMappingProfile()
        {
            CreateMap<ApplyForVacancy, CandidateApplicationStatus>();
            CreateMap<CandidateApplicationStatus, ApplicationStatusModel>();
            CreateMap<CreateCandidate, Candidate>();
            CreateMap<Candidate, CandidateModel>();
        }
    }
}
