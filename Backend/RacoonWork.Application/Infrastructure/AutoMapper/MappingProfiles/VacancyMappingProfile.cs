﻿using AutoMapper;
using RacoonWork.Application.Vacancies.Commands;
using RacoonWork.Application.Vacancies.Models;
using RacoonWork.Domain.Entities;

namespace RacoonWork.Application.Infrastructure.AutoMapper.MappingProfiles
{
    class VacancyMappingProfile : Profile
    {
        public VacancyMappingProfile()
        {
            CreateMap<Vacancy, VacancyModel>();
            CreateMap<СreateVacancy, Vacancy>();
        }
    }
}
