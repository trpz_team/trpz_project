﻿using AutoMapper;
using RacoonWork.Application.Companies.Commands;
using RacoonWork.Application.Companies.Models;
using RacoonWork.Domain.Entities;

namespace RacoonWork.Application.Infrastructure.AutoMapper.MappingProfiles
{
    class CompanyMappingProfile : Profile
    {
        public CompanyMappingProfile()
        {
            CreateMap<CreateCompany, Company>();
            CreateMap<Company, CompanyModel>();
            CreateMap<CompanyModel, Company>();
        }
    }
}
