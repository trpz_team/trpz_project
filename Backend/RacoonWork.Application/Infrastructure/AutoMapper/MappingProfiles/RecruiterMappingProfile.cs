﻿using AutoMapper;
using RacoonWork.Application.Recruiters.Commands;
using RacoonWork.Application.Recruiters.Models;
using RacoonWork.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Application.Infrastructure.AutoMapper.MappingProfiles
{
    class RecruiterMappingProfile : Profile
    {
        public RecruiterMappingProfile()
        {
            CreateMap<CreateRecruiter, Recruiter>();
            CreateMap<Recruiter, RecruiterModel>();
        }
    }
}
