﻿using AutoMapper;
using RacoonWork.Application.JobTypes.Commands;
using RacoonWork.Application.JobTypes.Models;
using RacoonWork.Domain.Entities;

namespace RacoonWork.Application.Infrastructure.AutoMapper.MappingProfiles
{
    class JobTypesMappingProfile : Profile
    {
        public JobTypesMappingProfile()
        {
            CreateMap<CreateJobType, JobType>();
            CreateMap<JobType, JobTypeModel>();
        }
    }
}
