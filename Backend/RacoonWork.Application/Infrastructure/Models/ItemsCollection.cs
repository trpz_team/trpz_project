﻿using System.Collections.Generic;

namespace RacoonWork.Application.Infrastructure.Models
{
    public class ItemsCollection<T>
    {
        public IEnumerable<T> Items { get; set; }
    }
}
