﻿using RacoonWork.Application.Infrastructure.Models;

namespace RacoonWork.Application.JobTypes.Models
{
    public class JobTypeListModel : ItemsCollection<JobTypeModel>
    {
    }
}
