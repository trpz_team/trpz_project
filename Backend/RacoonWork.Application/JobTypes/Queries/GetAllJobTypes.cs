﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.JobTypes.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace RacoonWork.Application.JobTypes.Queries
{
    public class GetAllJobTypes : IRequest<JobTypeListModel>
    {
        public class Handler : IRequestHandler<GetAllJobTypes, JobTypeListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<JobTypeListModel> Handle(GetAllJobTypes request, CancellationToken cancellationToken)
            {
                var jobTypes = await dbContext.JobTypes.ToListAsync(cancellationToken);
                var mappedJobTypes = jobTypes.Select(x => mapper.Map<JobTypeModel>(x));
                return new JobTypeListModel { Items = mappedJobTypes };
            }
        }

    }
}
