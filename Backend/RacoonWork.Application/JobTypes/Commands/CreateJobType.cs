﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.JobTypes.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.JobTypes.Commands
{
    public class CreateJobType : IRequest<JobTypeModel>
    {
        public string Name { get; set; }

        public class Validator : AbstractValidator<CreateJobType>
        {
            public Validator()
            {
                RuleFor(x => x.Name).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateJobType, JobTypeModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<JobTypeModel> Handle(CreateJobType request, CancellationToken cancellationToken)
            {
                var jobType = mapper.Map<JobType>(request);
                dbContext.JobTypes.Add(jobType);
                await dbContext.SaveChangesAsync(cancellationToken);
                return mapper.Map<JobTypeModel>(jobType);
            }
        }
    }
}
