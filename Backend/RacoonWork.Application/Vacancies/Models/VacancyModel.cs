﻿namespace RacoonWork.Application.Vacancies.Models
{
    public class VacancyModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double RequiredExperience { get; set; }
        public decimal MaxSalary { get; set; }
        public bool VacancyIsOpen { get; set; }
        public string City { get; set; }
        public int RecruiterId { get; set; }
        public int JobTypeId { get; set; }
    }
}
