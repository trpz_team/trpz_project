﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Vacancies.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Vacancies.Commands
{
    public class СreateVacancy : IRequest<VacancyModel>
    {
        public string Title { get; set; }
        public double RequiredExperience { get; set; }
        public decimal MaxSalary { get; set; }
        public int JobTypeId { get; set; }
        public bool VacancyStatus { get; set; }
        public int RecruiterId { get; set; }

        public class Validator : AbstractValidator<СreateVacancy>
        {
            public Validator()
            {
                RuleFor(x => x.Title).NotEmpty();
                RuleFor(x => x.RequiredExperience).NotEmpty();
                RuleFor(x => x.MaxSalary).NotEmpty();
                RuleFor(x => x.JobTypeId).NotEmpty();
                RuleFor(x => x.VacancyStatus).NotEmpty();
                RuleFor(x => x.RecruiterId).NotEmpty();
            }
        }
        public class Handler : IRequestHandler<СreateVacancy, VacancyModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<VacancyModel> Handle(СreateVacancy request, CancellationToken cancellationToken)
            {
                var vacancy = mapper.Map<Vacancy>(request);
                dbContext.Vacancies.Add(vacancy);
                await dbContext.SaveChangesAsync(cancellationToken);
                return mapper.Map<VacancyModel>(vacancy);
            }
        }
    }
}
