﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Vacancies.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Vacancies.Queries
{
    public class GetVacancies:IRequest<VacanciesListModel>
    {
        public class Handler : IRequestHandler<GetVacancies, VacanciesListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }
            public async Task<VacanciesListModel> Handle(GetVacancies request, CancellationToken cancellationToken)
            {
                var allVacancies = await dbContext.Vacancies.ToListAsync(cancellationToken);
                var vacancyModels = allVacancies.Select(x => mapper.Map<VacancyModel>(x));
                return new VacanciesListModel { Items = vacancyModels };
            }
        }
    }
}
