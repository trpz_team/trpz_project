﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Companies.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Companies.Queries
{
    public class GetCompanies : IRequest<CompanyListModel>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetCompanies, CompanyListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompanyListModel> Handle(GetCompanies request, CancellationToken cancellationToken)
            {
                var companies =  await dbContext.Companies.ToListAsync(cancellationToken);
                var companyModels = companies.Select(company => mapper.Map<CompanyModel>(company));
                return new CompanyListModel { Items = companyModels };
            }
        }
    }
}
