﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Companies.Models;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Companies.Queries
{
    public class GetCompanyById : IRequest<CompanyModel>
    {
        public int Id { get; set; }

        public class Validator : AbstractValidator<GetCompanyById>
        {
            public Validator()
            {
                RuleFor(x => x.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetCompanyById, CompanyModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompanyModel> Handle(GetCompanyById request, CancellationToken cancellationToken)
            {
                var company = await dbContext.Companies.FindAsync(new object[] { request.Id }, cancellationToken);
                if (company == null)
                {
                    return null;
                }
                return mapper.Map<CompanyModel>(company);
            }
        }
    }
}
