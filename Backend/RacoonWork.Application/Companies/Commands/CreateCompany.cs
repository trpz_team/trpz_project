﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Companies.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Companies.Commands
{
    public class CreateCompany: IRequest<CompanyModel>
    {
        public string Name { get; set; }

        class Validator : AbstractValidator<CreateCompany>
        {
            public Validator()
            {
                RuleFor(x => x.Name).NotEmpty();
            }
        }
        class Handler : IRequestHandler<CreateCompany, CompanyModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<CompanyModel> Handle(CreateCompany request, CancellationToken cancellationToken)
            {
                var company = mapper.Map<Company>(request);
                this.dbContext.Add(company);
                await this.dbContext.SaveChangesAsync(cancellationToken);
                return mapper.Map<CompanyModel>(company);
            }
        }
    }
}
