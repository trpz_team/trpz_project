﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Application.Companies.Models
{
    public class CompanyModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
