﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Application.Recruiters.Models
{
    public class RecruiterModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int СompanyId { get; set; }
        public string HeadOfficeCity { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
