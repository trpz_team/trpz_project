﻿using AutoMapper;
using FluentValidation;
using MediatR;
using RacoonWork.Application.Recruiters.Models;
using RacoonWork.Domain.Entities;
using RacoonWork.Persistence;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Recruiters.Commands
{
    public class CreateRecruiter : IRequest<RecruiterModel>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int СompanyId { get; set; }

        public class Validator : AbstractValidator<CreateRecruiter>
        {
            public Validator()
            {
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.СompanyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<CreateRecruiter, RecruiterModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<RecruiterModel> Handle(CreateRecruiter request, CancellationToken cancellationToken)
            {
                var recruiter = mapper.Map<Recruiter>(request);
                this.dbContext.Recruiters.Add(recruiter);
                await dbContext.SaveChangesAsync(cancellationToken);
                return mapper.Map<RecruiterModel>(recruiter);
            }
        }
    }
}
