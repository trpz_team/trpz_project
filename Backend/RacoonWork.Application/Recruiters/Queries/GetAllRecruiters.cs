﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Recruiters.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Recruiters.Queries
{
    public class GetAllRecruiters : IRequest<RecruitersListModel>
    {
        public class Handler : IRequestHandler<GetAllRecruiters, RecruitersListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<RecruitersListModel> Handle(GetAllRecruiters request, CancellationToken cancellationToken)
            {
                var recruiters = await dbContext.Recruiters.ToListAsync(cancellationToken);
                var mappedRecruiters = recruiters.Select(x => mapper.Map<RecruiterModel>(x));
                return new RecruitersListModel { Items = mappedRecruiters };
            }
        }
    }
}
