﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RacoonWork.Application.Recruiters.Models;
using RacoonWork.Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RacoonWork.Application.Recruiters.Queries
{
    public class GetRecruitersByCompanyId : IRequest<RecruitersListModel>
    {
        public int CompanyId { get; set; }

        public class Validator : AbstractValidator<GetRecruitersByCompanyId>
        {
            public Validator()
            {
                RuleFor(x => x.CompanyId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<GetRecruitersByCompanyId, RecruitersListModel>
        {
            private readonly RacoonWorkContext dbContext;
            private readonly IMapper mapper;

            public Handler(RacoonWorkContext dbContext, IMapper mapper)
            {
                this.dbContext = dbContext;
                this.mapper = mapper;
            }

            public async Task<RecruitersListModel> Handle(GetRecruitersByCompanyId request, CancellationToken cancellationToken)
            {
                var recruiters = await dbContext.Recruiters
                    .Where(x => x.СompanyId == request.CompanyId)
                    .ToListAsync(cancellationToken);

                var mappedRecruiters = recruiters.Select(x => mapper.Map<RecruiterModel>(x));
                return new RecruitersListModel { Items = mappedRecruiters };
            }
        }

    }
}
