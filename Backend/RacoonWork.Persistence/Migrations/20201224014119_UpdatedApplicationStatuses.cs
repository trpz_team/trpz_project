﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RacoonWork.Persistence.Migrations
{
    public partial class UpdatedApplicationStatuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateApplicationStatuses_ApplicationStatus_ApplicationStatusId",
                table: "CandidateApplicationStatuses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationStatus",
                table: "ApplicationStatus");

            migrationBuilder.RenameTable(
                name: "ApplicationStatus",
                newName: "ApplicationStatuses");

            migrationBuilder.RenameColumn(
                name: "VacancyStatus",
                table: "Vacancies",
                newName: "VacancyIsOpen");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationStatuses",
                table: "ApplicationStatuses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateApplicationStatuses_ApplicationStatuses_ApplicationStatusId",
                table: "CandidateApplicationStatuses",
                column: "ApplicationStatusId",
                principalTable: "ApplicationStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateApplicationStatuses_ApplicationStatuses_ApplicationStatusId",
                table: "CandidateApplicationStatuses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationStatuses",
                table: "ApplicationStatuses");

            migrationBuilder.RenameTable(
                name: "ApplicationStatuses",
                newName: "ApplicationStatus");

            migrationBuilder.RenameColumn(
                name: "VacancyIsOpen",
                table: "Vacancies",
                newName: "VacancyStatus");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationStatus",
                table: "ApplicationStatus",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateApplicationStatuses_ApplicationStatus_ApplicationStatusId",
                table: "CandidateApplicationStatuses",
                column: "ApplicationStatusId",
                principalTable: "ApplicationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
