﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RacoonWork.Persistence.Migrations
{
    public partial class AddedCandidateId2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_CandidateCvs_CandidateId",
                table: "CandidateCvs",
                column: "CandidateId");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateCvs_Candidates_CandidateId",
                table: "CandidateCvs",
                column: "CandidateId",
                principalTable: "Candidates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateCvs_Candidates_CandidateId",
                table: "CandidateCvs");

            migrationBuilder.DropIndex(
                name: "IX_CandidateCvs_CandidateId",
                table: "CandidateCvs");
        }
    }
}
