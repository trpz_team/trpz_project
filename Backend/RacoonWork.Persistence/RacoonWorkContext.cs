﻿using Microsoft.EntityFrameworkCore;
using RacoonWork.Domain.Entities;
using System.Reflection;

namespace RacoonWork.Persistence
{
    public partial class RacoonWorkContext : DbContext
    {
        public RacoonWorkContext()
        {
        }

        public RacoonWorkContext(DbContextOptions<RacoonWorkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CandidateVacancies> CandidateVacancies { get; set; }
        public virtual DbSet<Candidate> Candidates { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<JobType> JobTypes { get; set; }
        public virtual DbSet<Recruiter> Recruiters { get; set; }
        public virtual DbSet<Vacancy> Vacancies { get; set; }
        public virtual DbSet<CandidateApplicationStatus> CandidateApplicationStatuses { get; set; }
        public virtual DbSet<CandidateCv> CandidateCvs { get; set; }
        public virtual DbSet<ApplicationStatus> ApplicationStatuses { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
