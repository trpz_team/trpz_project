﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RacoonWork.Domain.Entities;

namespace RacoonWork.Persistence.Configurations
{
    class VacancyConfiguration : IEntityTypeConfiguration<Vacancy>
    {
        public void Configure(EntityTypeBuilder<Vacancy> builder)
        {
            builder.Property(e => e.MaxSalary).HasColumnType("money");

            builder.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(100);

            builder.HasOne(d => d.JobType)
                .WithMany(p => p.Vacancies)
                .HasForeignKey(d => d.JobTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Vacancies_JobTypes");

            builder.HasOne(d => d.Recruiter)
                .WithMany(p => p.Vacancies)
                .HasForeignKey(d => d.RecruiterId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Vacancies_Recruiters");
        }
    }
}
