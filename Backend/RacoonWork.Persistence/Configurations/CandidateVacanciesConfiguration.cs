﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RacoonWork.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Persistence.Configurations
{
    class CandidateVacanciesConfiguration : IEntityTypeConfiguration<CandidateVacancies>
    {
        public void Configure(EntityTypeBuilder<CandidateVacancies> builder)
        {
            builder.HasOne(d => d.Candidate)
                    .WithMany(p => p.CandidateVacancies)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CandidateVacancies_Candidates");

            builder.HasOne(d => d.Vacancy)
                .WithMany(p => p.CandidateVacancies)
                .HasForeignKey(d => d.VacancyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CandidateVacancies_Vacancies");
        }
    }
}
