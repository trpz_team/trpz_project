﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RacoonWork.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Persistence.Configurations
{
    class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.Property(e => e.Name)
                     .IsRequired()
                     .HasMaxLength(100);
        }
    }
}
