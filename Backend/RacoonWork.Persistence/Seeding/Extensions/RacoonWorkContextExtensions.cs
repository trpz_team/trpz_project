﻿using Microsoft.EntityFrameworkCore;
using RacoonWork.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RacoonWork.Persistence.Seeding.Extensions
{

    public static class RacoonWorkContextExtensions
    {

        public static  void Seed(this RacoonWorkContext ctx)
        {
            if (! ctx.Candidates.Any())
            {
                ctx.Candidates.AddRange(new Candidate
                {
                    FirstName = "Yuriy",
                    LastName = "Gagarin",
                    Email = "yuriy.gagarin@gmail.com",
                    City = "Moscow"
                },
                new Candidate
                {
                    FirstName = "John F.",
                    LastName = "Kennedy",
                    Email = "john.kennedy@gmail.com",
                    City = "Washington"
                },
                new Candidate
                {
                    FirstName = "Jason",
                    LastName = "Bourne",
                    Email = "jason.bourne@gmail.com",
                    City = "New York"
                });
                 ctx.SaveChanges();
            }
            if (! ctx.CandidateCvs.Any())
            {
                ctx.CandidateCvs.AddRange(
                    new CandidateCv
                    {
                        Position = "Junior Dev",
                        CvUrl = "https://google.com",
                        CandidateId = 1
                    },
                    new CandidateCv
                    {
                        Position = "Senior Dev",
                        CvUrl = "https://google.com",
                        CandidateId = 2

                    },
                    new CandidateCv
                    {
                        Position = "PM",
                        CvUrl = "https://google.com",
                        CandidateId = 3
                    });
                 ctx.SaveChanges();
            }
            if (! ctx.Companies.Any())
            {
                ctx.Companies.AddRange(new Company
                {
                    Name = "Company 1"
                },
                new Company
                {
                    Name = "Company 2"
                },
                new Company
                {
                    Name = "Company 3"
                });
                 ctx.SaveChanges();
            }
            if (! ctx.JobTypes.Any())
            {
                ctx.JobTypes.AddRange(
                    new JobType { Name = "Remote" }, 
                    new JobType { Name = "Office" }
                );
                 ctx.SaveChanges();
            }
            if(! ctx.Recruiters.Any())
            {
                ctx.Recruiters.AddRange(
                    new Recruiter
                    {
                        FirstName="John",
                        LastName="Doe",
                        СompanyId = 1,
                        HeadOfficeCity = "Kiev",
                        Email="john.doe@gmail.com",
                        PhoneNumber = "1234567890"
                    },
                    new Recruiter
                    {
                        FirstName = "Jane",
                        LastName = "Doe",
                        СompanyId = 2,
                        HeadOfficeCity = "Moscow",
                        Email = "jane.doe@gmail.com",
                        PhoneNumber = "1234567890"
                    });
                 ctx.SaveChanges();
            }
            if(! ctx.Vacancies.Any())
            {
                ctx.Vacancies.AddRange(
                new Vacancy
                {
                    Title="Junior Java Dev",
                    RequiredExperience = 1,
                    MaxSalary = 500,
                    VacancyIsOpen = true,
                    City = "Kiev",
                    RecruiterId = 1,
                    JobTypeId = 2
                },
                new Vacancy
                {
                    Title = "Senior .NET Dev",
                    RequiredExperience = 1,
                    MaxSalary = 3000,
                    VacancyIsOpen = false,
                    City = "London",
                    RecruiterId = 2,
                    JobTypeId = 1
                });
                 ctx.SaveChanges();
            }
            if (! ctx.ApplicationStatuses.Any())
            {
                ctx.ApplicationStatuses.AddRange(new ApplicationStatus
                {
                    Name = "Approved"
                },
                new ApplicationStatus
                {
                    Name = "Rejected"
                });
                 ctx.SaveChanges();
            }
            if (! ctx.CandidateApplicationStatuses.Any())
            {
                ctx.CandidateApplicationStatuses.AddRange(new CandidateApplicationStatus
                {
                    VacancyId = 1,
                    CandidateId = 1,
                    ApplicationStatusId = 1,
                    CandidatePhoneNumber = "1234567890",
                    CandidateExperienceInYears = 1
                },
                new CandidateApplicationStatus
                {
                    VacancyId = 1,
                    CandidateId = 2,
                    ApplicationStatusId = 2,
                    CandidatePhoneNumber = "1234567890",
                    CandidateExperienceInYears = 2
                },
                new CandidateApplicationStatus
                {
                    VacancyId = 1,
                    CandidateId = 3,
                    ApplicationStatusId = 2,
                    CandidatePhoneNumber = "1234567890",
                    CandidateExperienceInYears = 0
                },
                new CandidateApplicationStatus
                {
                    VacancyId = 2,
                    CandidateId = 2,
                    ApplicationStatusId = 1,
                    CandidatePhoneNumber = "1234567890",
                    CandidateExperienceInYears = 4
                });
                 ctx.SaveChanges();
            }
            //if (! ctx.CandidateVacancies.Any())
            //{
            //    ctx.CandidateVacancies.AddRange(
            //        new CandidateVacancies
            //        {
            //            CandidateId = 1,
            //            VacancyId = 1
            //        },
            //        new CandidateVacancies
            //        {
            //            CandidateId = 1,
            //            VacancyId = 2
            //        },
            //        new CandidateVacancies
            //        {
            //            CandidateId = 1,
            //            VacancyId = 3
            //        },
            //        new CandidateVacancies
            //        {
            //            CandidateId = 2,
            //            VacancyId = 2
            //        });
            //     ctx.SaveChanges();
            //}
        }
    }
}
