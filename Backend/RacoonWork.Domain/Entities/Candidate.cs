﻿using System;
using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public  class Candidate
    {
        public Candidate()
        {
            CandidateVacancies = new HashSet<CandidateVacancies>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }

        public virtual ICollection<CandidateVacancies> CandidateVacancies { get; set; }
    }
}
