﻿using System;
using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public class Vacancy
    {
        public Vacancy()
        {
            CandidateVacancies = new HashSet<CandidateVacancies>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public double RequiredExperience { get; set; }
        public decimal MaxSalary { get; set; }
        public bool VacancyIsOpen { get; set; }
        public string City { get; set; }
        public int RecruiterId { get; set; }
        public int JobTypeId { get; set; }
        public virtual JobType JobType { get; set; }
        public virtual Recruiter Recruiter { get; set; }
        public virtual ICollection<CandidateVacancies> CandidateVacancies { get; set; }
    }
}
