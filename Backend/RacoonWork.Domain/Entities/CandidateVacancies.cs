﻿using System;
using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public class CandidateVacancies
    {
        public int Id { get; set; }
        public int CandidateId { get; set; }
        public int VacancyId { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual Vacancy Vacancy { get; set; }
    }
}
