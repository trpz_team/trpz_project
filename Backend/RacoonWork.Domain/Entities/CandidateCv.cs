﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Domain.Entities
{
    public class CandidateCv
    {
        public int Id { get; set; }

        public string Position { get; set; }

        public string CvUrl { get; set; }

        public int CandidateId { get; set; }

        public Candidate Candidate { get; set; }
    }
}
