﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RacoonWork.Domain.Entities
{
    public class CandidateApplicationStatus
    {
        public int Id { get; set; }
        public int VacancyId { get; set; }
        public int CandidateId { get; set; }
        public int ApplicationStatusId { get; set; }

        public string CandidatePhoneNumber { get; set; }

        public double CandidateExperienceInYears { get; set; }

        public Candidate Candidate { get; set; }
        public Vacancy Vacancy { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
    }
}
