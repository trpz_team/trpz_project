﻿using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public partial class Recruiter
    {
        public Recruiter()
        {
            Vacancies = new HashSet<Vacancy>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int СompanyId { get; set; }
        public string HeadOfficeCity { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual Company Сompany { get; set; }
        public virtual ICollection<Vacancy> Vacancies { get; set; }
    }
}
