﻿namespace RacoonWork.Domain.Entities
{
    public class ApplicationStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
