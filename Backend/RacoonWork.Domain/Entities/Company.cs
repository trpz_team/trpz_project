﻿using System;
using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public class Company
    {
        public Company()
        {
            Recruiters = new HashSet<Recruiter>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Recruiter> Recruiters { get; set; }
    }
}
