﻿using System;
using System.Collections.Generic;

namespace RacoonWork.Domain.Entities
{
    public class JobType
    {
        public JobType()
        {
            Vacancies = new HashSet<Vacancy>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Vacancy> Vacancies { get; set; }
    }
}
