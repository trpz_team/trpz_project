﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RacoonWork.Application.Recruiters.Commands;
using RacoonWork.Application.Recruiters.Models;
using RacoonWork.Application.Recruiters.Queries;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Controllers.v1
{
    [Authorize]
    public class RecruitersController : ApiController
    {
        public RecruitersController(IMediator mediator) : base(mediator)
        {
        }

        [HttpPost]
        public async Task<ActionResult<RecruiterModel>> CreateRecruiter(CreateRecruiter request)
        {
            var createdRecruiter = await Mediator.Send(request);
            return CreatedAtAction(nameof(GetRecruiters), createdRecruiter);
        }

        [HttpGet]
        public async Task<ActionResult<RecruitersListModel>> GetRecruiters()
        {
            return Ok(await Mediator.Send(new GetAllRecruiters()));
        }

        [HttpGet]
        [Route("recruitersByCompany/{id}")]
        public async Task<ActionResult<RecruitersListModel>> GetRecruitersByCompanyId(int id)
        {
            return Ok(await Mediator.Send(new GetRecruitersByCompanyId { CompanyId = id }));
        }
    }
}
