﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RacoonWork.Application.JobTypes.Commands;
using RacoonWork.Application.JobTypes.Models;
using RacoonWork.Application.JobTypes.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Controllers.v1
{
    [Authorize]
    public class JobTypesController : ApiController
    {
        public JobTypesController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        public async Task<ActionResult<JobTypeListModel>> GetAllJobTypes()
        {
            return Ok(await Mediator.Send(new GetAllJobTypes()));
        }

        [HttpPost]
        public async Task<ActionResult<JobTypeModel>> CreateJobType(CreateJobType request)
        {
            var createdJobType = await Mediator.Send(request);
            return CreatedAtAction(nameof(GetAllJobTypes), createdJobType);
        }
    }
}
