﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RacoonWork.Application.Candidates.Commands;
using RacoonWork.Application.Candidates.Models;
using RacoonWork.Application.Candidates.Queries;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Controllers.v1
{
    [Authorize]
    public class CandidatesController : ApiController
    {
        public CandidatesController(IMediator mediator):
            base(mediator)
        {
        }

        [HttpGet]
        public async Task<ActionResult<CandidateListModel>> GetAllCandidates()
        {
            return Ok(await Mediator.Send(new GetAllCandidates()));
        }
        [HttpGet]
        [Route("candidates/{id}/applicationStatus")]
        public async Task<ActionResult<ApplicationsListModel>> GetAllAplications(int id)
        {
            return Ok(await Mediator.Send(new GetAllApplications { CandidateId = id }));
        }

        [HttpGet]
        [Route("candidates/{id}/applicationStatus/{vacancyId}")]
        public async Task<ActionResult<ApplicationStatusModel>> GetApplicationStatus(int id, int vacancyId)
        {
            var query = new GetApplicationStatus { CandidateId = id, VacancyId = vacancyId };

            return Ok(await Mediator.Send(query));
        }

        [HttpPost]
        [Route("candidates/{id}/applyForVacancy/{vacancyId})")]
        public async Task<ActionResult<ApplicationStatusModel>> ApplyForVacancy(int id, int vacancyId)
        {
            return Ok(await Mediator.Send(new ApplyForVacancy { CandidateId = id, VacancyId = vacancyId }));
        }

        [HttpPost]
        public async Task<ActionResult<CandidateModel>> CreateCandidate(CreateCandidate request)
        {
            var createdCandidate = await Mediator.Send(request);
            return CreatedAtAction(nameof(GetAllCandidates), createdCandidate);
        }
    }
}
