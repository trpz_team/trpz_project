﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RacoonWork.Application.Vacancies.Commands;
using RacoonWork.Application.Vacancies.Models;
using RacoonWork.Application.Vacancies.Queries;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Controllers.v1
{
    [Authorize]
    public class VacanciesController : ApiController
    {
        public VacanciesController(IMediator mediator)
            : base(mediator)
        {
        }

        [HttpGet]
        public async Task<ActionResult<VacanciesListModel>> GetAllVacancies()
        {
            return Ok(await Mediator.Send(new GetVacancies()));
        }

        [HttpPost]
        public async Task<ActionResult<VacancyModel>> CreateVacancy(СreateVacancy request)
        {
            var createdVacancy = await Mediator.Send(request);
            return CreatedAtAction(nameof(GetAllVacancies), createdVacancy);
        }
    }
}
