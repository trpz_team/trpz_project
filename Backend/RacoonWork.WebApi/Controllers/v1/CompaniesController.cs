﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RacoonWork.Application.Companies.Commands;
using RacoonWork.Application.Companies.Models;
using RacoonWork.Application.Companies.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Controllers.v1
{
    [Authorize]
    public class CompaniesController : ApiController
    {
        public CompaniesController(IMediator mediator)
            : base(mediator)
        {
        }

        [HttpGet]
        public async Task<ActionResult<CompanyListModel>> GetAllCompanies()
        {
            return Ok(await Mediator.Send(new GetCompanies()));
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<CompanyListModel>> GetCompanyById(int id)
        {
            return Ok(await Mediator.Send(new GetCompanyById { Id = id }));
        }

        [HttpPost]
        public async Task<ActionResult<CompanyModel>> CreateCompany(CreateCompany request)
        {
            var createdCompany = await Mediator.Send(request);
            return CreatedAtAction(nameof(GetCompanyById), new { id = createdCompany.Id }, createdCompany);
        }
    }
}
