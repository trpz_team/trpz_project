using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RacoonWork.Application.Candidates.Commands;
using RacoonWork.Application.Companies.Queries;
using RacoonWork.Application.Infrastructure.AutoMapper.Extensions;
using RacoonWork.Persistence;
using RacoonWork.WebApi.Infrastructure.Authentication;
using RacoonWork.WebApi.Infrastructure.Data;
using RacoonWork.WebApi.Infrastructure.Swagger.Extensions;
using RacoonWork.WebApi.Infrastructure.Swagger.Options;

namespace RacoonWork.WebApi
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            this._env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mediatrAssembly = typeof(CreateCandidate).GetTypeInfo().Assembly;
            services.AddMediatR(mediatrAssembly);
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddCustomAutoMapper();
            services.AddControllers();

            services.AddCustomAuthentication(Configuration);
            var applicationAssembly = typeof(GetCompanies).GetTypeInfo().Assembly;
            services.AddDbContext<RacoonWorkContext>(options =>
                 options.UseSqlServer(Configuration["Db:ConnectionString"]));

            services.AddOptions<ClientOAuth2Options>()
        .Bind(Configuration.GetSection(ClientOAuth2Options.Section));
            services.AddSwagger();
            #region Api Versioning
            services.AddApiVersioning(
               options =>
               {
                   // use api v1.0 by default
                   options.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                   options.AssumeDefaultVersionWhenUnspecified = true;
                   // if not specified in header 
                   options.ApiVersionReader = new HeaderApiVersionReader("api-version");
                   options.ReportApiVersions = true;
               });

            services.AddVersionedApiExplorer(
                options =>
                {
                    //Format: vMajorVersion.MinorVersion
                    options.GroupNameFormat = "'v'VV";
                    options.SubstituteApiVersionInUrl = true;
                });
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseHttpsRedirection();
            app.MigrateDatabase(Configuration);
            app.SeedDatabase(Configuration);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCustomSwagger();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
