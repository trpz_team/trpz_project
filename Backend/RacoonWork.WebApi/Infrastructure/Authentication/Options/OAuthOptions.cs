﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RacoonWork.WebApi.Infrastructure.Authentication.Options
{
    public class OAuthOptions
    {
        public static string Section => "OAuth2";

        public string Domain { get; set; }

        public string Audience { get; set; }

        public int CacheTokenSec { get; set; }
    }
}
